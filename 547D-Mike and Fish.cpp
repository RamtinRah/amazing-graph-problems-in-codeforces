///link to problem : https://codeforces.com/problemset/problem/547/D





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 5;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
set<pii> st[2 * maxn];
bool ans[maxn];
void dfs(int v , bool b){
    if(st[v].size() == 0)
        return ;
    int u = (*st[v].begin()).first;
    int id = (*st[v].begin()).second;
    ans[id] = b;
    st[v].erase(st[v].begin());
    st[u].erase(mp(v , id));
    dfs(u , b ^ 1);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n;
    cin >> n;
    for(int i=0;i<n;i++){
        int x , y;
        cin >> x >> y;
        y += maxn;
        st[x].insert(mp(y , i + 1)) , st[y].insert(mp(x , i + 1));
    }
    vector<int> v1 , v2;
    for(int i=1;i<maxn;i++)
        if(st[i].size() % 2)
            v1.pb(i);
    for(int i=maxn;i<2*maxn;i++)
        if(st[i].size() % 2)
            v2.pb(i);
    if(v1.size() % 2){
        int x = v1.back() , y = v2.back();
        v1.pop_back() ,v2.pop_back();
        st[x].insert(mp(y , 0)) , st[y].insert(mp(x , 0));
    }
    for(int i=0;i<v1.size();i++){
        int x = v1[i];
        st[x].insert(mp(2 * maxn - 1 , 0));
        st[2 * maxn - 1].insert(mp(x , 0));
    }
    for(int i=0;i<v2.size();i++){
        int x = v2[i];
        st[x].insert(mp(0 , 0));
        st[0].insert(mp(x , 0));
    }
    for(int i=0;i<2*maxn;i++)
        dfs(i , 0);
    for(int i=1;i<=n;i++)
        if(ans[i])
            cout << "b";
        else
            cout << "r";
    return 0;
}
