
///link to problem : https://codeforces.com/problemset/problem/659/F






#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e3 + 3;
const int maxlog = 20;
const int inf = 1e9 + 4;
int a[maxn][maxn];
int n , m;
int par[maxn * maxn] , sz[maxn * maxn];
int ans[maxn][maxn];
bool mark[maxn * maxn];
struct cell{
    int val , x , y;
} b[maxn * maxn];
bool cmp(cell a , cell b){
    return a . val > b . val;
}
bool ok(int x , int y){
    if(x > -1 && y > -1 && x < n && y < m && mark[x * m + y] && !ans[x][y])
        return true;
    else
        return false;
}
int get_par(int x){
    if(x == par[x])
        return x;
    return par[x] = get_par(par[x]);
}
void merge(int x1 , int y1 , int x2 , int y2){
    int p1 = get_par(x1 * m + y1);
    int p2 = get_par(x2 * m + y2);
    if(p1 == p2)
        return ;
    sz[p2] += sz[p1];
    par[p1] = p2;
}
void add(cell c){
    int x = c.x , y = c.y , val = c.val;
    par[x * m + y] = x * m + y;
    sz[x * m + y] = 1;
    mark[x * m + y] = true;
    if(ok(x - 1 , y))
        merge(x , y , x - 1 , y);
    if(ok(x + 1 , y))
        merge(x , y , x + 1 , y);
    if(ok(x , y - 1))
        merge(x , y , x , y - 1);
    if(ok(x , y + 1))
        merge(x , y , x , y + 1);
}
ll c = 0;
void dfs(int x , int y , int val){
    if(! c)
        return ;
    c -- ;
    ans[x][y] = val;
    if(ok(x - 1 , y))
        dfs(x - 1 , y , val);
    if(ok(x + 1 , y))
        dfs(x + 1 , y , val);
    if(ok(x , y + 1))
        dfs(x , y + 1 , val);
    if(ok(x , y - 1))
        dfs(x , y - 1 , val);
}
void print(bool b){
    if(!b){
        cout << "NO";
        return ;
    }
    cout << "YES" << endl;
    for(int i = 0 ; i < n ; i ++ ){
        for(int j = 0 ; j < m ; j ++ )
            cout << ans[i][j] << " ";
        cout << endl;
    }
}
int main()
{
	ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0);
	ll k;
	cin >> n >> m >> k;
	for(int i = 0 ; i < n ; i ++ ){
        for(int j = 0 ; j < m ; j ++ ){
            cin >> a[i][j];
            b[i * m + j] . val = a[i][j] , b[i * m + j] . x = i , b[i * m + j] . y = j;
        }
	}
	sort(b , b + n * m , cmp);
	for(int i = 0 ; i < n * m ; i ++ ){
        add(b[i]);
        ll val = (ll) b[i].val;
        int x = b[i] . x , y = b[i].y;
        int p = get_par(x * m + y);
        if(k % val == 0 && k <= (ll) sz[p] * val){
            c = k / val;
            dfs(x , y , val);
            print(1);
            return 0;
        }
	}
	print(0);
	return 0;
}
