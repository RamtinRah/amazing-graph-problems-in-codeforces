
///link to problem : https://codeforces.com/problemset/problem/609/E




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 4;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
const ll inf = 1e15 + 5;
int n , m;
vector<pair<int,pii> > edges;
vector<int> edge[maxn];
int pr[maxn] , depth[maxn];
map<pii,int> w;
int par[maxn][maxlog + 1];
int maxi[maxn][maxlog + 1];
pair<pii , int> e[maxn];
ll sum;
int get_par(int v){
    if(v == pr[v])
        return v;
    return pr[v] = get_par(pr[v]);
}
void kruskal(){
    sort(edges.begin() , edges.end());
    for(int i=1;i<=n;i++)
        pr[i] = i;
    for(int i=0;i<edges.size();i++){
        int u = edges[i].second.first;
        int v = edges[i].second.second;
        int p1 = get_par(u) , p2 = get_par(v);
        if(p1 != p2){
            pr[p1] = p2;
            edge[u].pb(v);
            edge[v].pb(u);
            sum += (ll) edges[i].first;
        }
    }
}
void dfs(int v , int p)
{
    par[v][0] = p;
    maxi[v][0] = w[mp(v , p)];
    depth[v] = depth[p] + 1;
    for(int i=1;i<maxlog;i++)
    {
        int u = par[v][i-1];
        par[v][i] = par[u][i-1];
        maxi[v][i] = max(maxi[v][i - 1] , maxi[u][i - 1]);
    }
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(u != p)
            dfs(u,v);
    }
}
int get(int u,int v)
{
    if(depth[u] < depth[v])
        swap(u,v);
    int ans = 0;
    for(int i=maxlog;i>=0;i--)
    {
        if(depth[par[u][i]] >= depth[v]){
            ans = max(ans , maxi[u][i]);
            u = par[u][i];
        }
    }
    if(u == v)
        return ans;
    for(int i=maxlog;i>=0;i--)
    {
        if(par[u][i] != par[v][i])
        {
            ans = max(ans , maxi[u][i]);
            ans = max(ans , maxi[v][i]);
            u = par[u][i];
            v = par[v][i];
        }
    }
    ans = max(ans , maxi[u][0]);
    ans = max(ans , maxi[v][0]);
    return ans;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n >> m;
    for(int i=0;i<m;i++){
        int u , v , we;
        cin >> u >> v >> we;
        edges.pb(mp(we , mp(u , v)));
        w[mp(u , v)] = we , w[mp(v , u)] = we;
        e[i] = mp(mp(u , v) , we);
    }
    kruskal();
    dfs(1 , 0);
    for(int i=0;i<m;i++){
        int u = e[i].first.first;
        int v = e[i].first.second;
        int we = e[i].second;
        int ma = get(u , v);
        cout << sum - (ll) ma + (ll) we << endl;
    }
    return 0;
}
