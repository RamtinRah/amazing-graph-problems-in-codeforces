
///link to problem : https://codeforces.com/problemset/problem/141/D






#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int,int>::iterator setit;
const int maxn=2e5+6;
const int inf=1e18;
map<int,vector<pii> > edge;
map<pii,int> m;
vector<int> points;
set<pair<ll,int> > st;
map<int,ll> dis;
map<int,int> pr , way;
map<int,vector<int> > vec;
void dij()
{
    for(int i=0;i<points.size();i++)
    {
        dis[points[i]] = inf;
        pr[points[i]] = -1;
    }
    dis[0] = 0;
    st.insert(mp(0,0));
    while(!st.empty())
    {
        int v = (*st.begin()).second;
        st.erase(st.begin());
        for(int i=0;i<edge[v].size();i++)
        {
            int u = edge[v][i].first;
            ll w = (ll) edge[v][i].second;
            if(dis[u] > dis[v] + w)
            {
                pr[u] = v;
                way[u] = vec[v][i];
                st.erase(mp(dis[u],u));
                dis[u] = dis[v] + w;
                st.insert(mp(dis[u],u));
            }
        }
    }
}
vector<int> ans;
void print(int v)
{
    if(v == -1 || pr[v] == -1)
        return ;
    if(way[v] != -1)
        ans.pb(way[v]);
    print(pr[v]);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n,l; cin >> n >> l;
    points.pb(0);
    points.pb(l);
    for(int i=0;i<n;i++)
    {
        int x , d , t , p;
        cin >> x >> d >> t >> p;
        if(x - p < 0)
            continue ;
        points.pb(x - p);
        points.pb(x + d);
        edge[x-p].pb(mp(x+d,p+t));
        vec[x-p].pb(i+1);
        m[mp(x-p,x+d)] = i+1;
        m[mp(x+d,x-p)] = i+1;
    }
    sort(points.begin(),points.end());
    for(int i=1;i<points.size();i++)
    {
        int u = points[i-1];
        int v = points[i];
        if(u != v)
        {
            edge[u].pb(mp(v,v-u));
            edge[v].pb(mp(u,v-u));
            vec[u].pb(-1);
            vec[v].pb(-1);
        }
    }
    dij();
    cout << dis[l] << endl;
    print(l);
    cout << ans.size() << endl;
    reverse(ans.begin(),ans.end());
    for(int i=0;i<ans.size();i++)
        cout << ans[i] << " ";
    return 0;
}
