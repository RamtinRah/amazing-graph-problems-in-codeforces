
///link to problem : https://codeforces.com/problemset/problem/553/C


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e5 + 5;
const ll inf = 1e18;
const int mod = 1e9+7;
vector<int> edge[maxn];
vector<pii> bad_edge;
bool mark[maxn] , vis[maxn] , box[maxn];
int comp[maxn] ;
int c = 0;
vector<int> adj[maxn];
void dfs(int v)
{
    mark[v] = true;
    comp[v] = c;
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(!mark[u])
            dfs(u);
    }
}
void dfs(int v , bool b)
{
    vis[v] = true;
    box[v] = b;
    for(int i=0;i<adj[v].size();i++)
    {
        int u = adj[v][i];
        if(vis[u] && box[u] == box[v])
        {
            cout << 0;
            exit(0);
        }
        if(!vis[u])
            dfs(u , b ^ 1);
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n , m;
    cin >> n >> m;
    for(int i=0;i<m;i++)
    {
        int u , v , c;
        cin >> u >> v >> c;
        if(c == 1){
            edge[u].pb(v);
            edge[v].pb(u);
        }
        else
            bad_edge.pb(mp(u , v));
    }
    for(int i=1;i<=n;i++)
        if(!mark[i]){
            c ++ ;
            dfs(i);
        }
    for(int i=0;i<bad_edge.size();i++)
    {
        int u = bad_edge[i].first;
        int v = bad_edge[i].second;
        int c1 = comp[u];
        int c2 = comp[v];
        adj[c1].pb(c2);
        adj[c2].pb(c1);
    }
    int ans = 1;
    for(int i=1;i<=c;i++)
    {
        if(vis[i])
            continue;
        if(i != 1)
            ans = (ans * 2) % mod;
        dfs(i , 0);
    }
    cout << ans ;
    return 0;
}
