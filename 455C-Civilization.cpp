///link to problem : https://codeforces.com/problemset/problem/455/C

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+7;
vector<int> edge[maxn] , vec;
int comp[maxn] , c , dis[maxn] , ans[maxn] ;

void dfs(int v , int p){
    comp[v] = c;
    dis[v] = dis[p] + 1;
    vec.pb(v);
    for(int i=0;i<edge[v].size();i++){
        int u = edge[v][i];
        if(u != p)
            dfs(u , v);
    }
}
int get_comp(int v){
    if(v == comp[v])
        return v;
    return comp[v] = get_comp(comp[v]);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m , q;
    scanf("%d %d %d" , & n , & m , & q);
    for(int i=0;i<m;i++){
        int u , v;
        scanf("%d %d" , & u , & v);
        edge[u].pb(v);
        edge[v].pb(u);
    }
    for(int i=1;i<=n;i++){
        if(!comp[i]){
            c = i;
            vec.clear();
            dfs(i , 0);
            int cur = i;
            for(int j=1;j<vec.size();j++)
                if(dis[vec[j]] > dis[cur])
                    cur = vec[j];
            vec.clear();
            dfs(cur , 0);
            int res = 0;
            for(int j=0;j<vec.size();j++)
                res = max(res , dis[vec[j]] - 1);
            ans[c] = res;
        }
    }
    while( q -- ){
        int type ;
        scanf("%d" , &type);
        if(type & 1){
            int v;
            scanf("%d" , &v);
            int c = get_comp(v);
            cout << ans[c] << endl;
        }
        else{
            int u , v;
            scanf("%d %d" , & u , & v);
            int c1 = get_comp(u) , c2 = get_comp(v);
            if(c1 == c2)
                continue;
            comp[c1] = c2;
            ans[c2] = max(max(ans[c1] , ans[c2]) , (ans[c1] + 1) / 2 + (ans[c2] + 1) / 2 + 1);
        }
    }
    return 0;
}
