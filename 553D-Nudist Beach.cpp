
///link to problem : https://codeforces.com/problemset/problem/553/D




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+7;
vector<int> edge[maxn] , vec;
bool bad[maxn];
ld val[maxn] , l[maxn];
set<pair<ld,int> > st;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m , k;
    cin >> n >> m >> k;
    for(int i=0;i<k;i++){
        int v;
        cin >> v;
        bad[v] = true;
    }
    for(int i=0;i<m;i++){
        int u , v;
        cin >> u >> v;
        edge[u].pb(v) , edge[v].pb(u);
    }
    for(int i=1;i<=n;i++){
        if(bad[i])
            continue;
        int res = 0;
        for(int j=0;j<edge[i].size();j++)
            if(!bad[edge[i][j]])
                res ++ ;
        ld d = (ld) res / (ld) edge[i].size();
        val[i] = d;
        st.insert(mp(d , i));
    }
    int cnt = 0;
    while(!st.empty()){
        int v = (*st.begin()).second;
        ld d = (*st.begin()).first;
        st.erase(st.begin());
        l[cnt] = d;
        cnt ++ ;
        vec.pb(v);
        bad[v] = true;
        for(int i=0;i<edge[v].size();i++){
            int u = edge[v][i];
            if(bad[u])
                continue;
            st.erase(mp(val[u] , u));
            val[u] -= 1 / (ld) edge[u].size();
            st.insert(mp(val[u] , u));
        }
    }
    int pl = 0;
    for(int i=1;i<n-k;i++)
        if(l[i] > l[pl])
            pl = i;
    cout << n - k - pl << endl;
    int i = n - k - 1;
    while(i >= pl){
        cout << vec[i] << " ";
        i -- ;
    }
    return 0;
}
