///link to problem : https://codeforces.com/problemset/problem/733/F


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 2e5 + 5;
const int maxlog = 25;
const ll mod = 1e9 + 7;
const int sq = 720;
const int inf = 1e9;
struct edge{
    int u , v , w , c , id;
} a[maxn];
vector<int> adj[maxn];
bool cmp(edge a , edge b){
    return a.w < b.w;
}
int comp[maxn];
int getcomp(int v){
    if(comp[v] == v)
        return v;
    return comp[v] = getcomp(comp[v]);
}
int maxi[maxn][maxlog] , par[maxn][maxlog] , depth[maxn] , maxid[maxn][maxlog];
void dfs(int v , int p , int t){
    depth[v] = depth[p] + 1;
    par[v][0] = p;
    for(int i = 1 ; i < maxlog ; i ++ )
        par[v][i] = par[par[v][i - 1]][i - 1];
    maxi[v][0] = a[t].w;
    maxid[v][0] = t;
    for(int i = 1 ; i < maxlog ; i ++ ){
        if(maxi[v][i - 1] > maxi[par[v][i - 1]][i - 1])
            maxid[v][i] = maxid[v][i - 1];
        else
            maxid[v][i] = maxid[par[v][i - 1]][i - 1];
        maxi[v][i] = max(maxi[v][i - 1] , maxi[par[v][i - 1]][i - 1]);
    }
    for(auto i : adj[v]){
        int u = a[i].u;
        if(u == v)
            u = a[i].v;
        if(u == p)
            continue;
        dfs(u , v , i);
    }
}
int get(int u , int v)
{
    if(depth[u] < depth[v])
        swap(u , v);
    int ans = 0 , ansid = -1;
    for(int i = maxlog - 1 ; i >= 0 ; i -- )
    {
        if(depth[par[u][i]] >= depth[v]){
            if(ans < maxi[u][i]){
                ans = maxi[u][i];
                ansid = maxid[u][i];
            }
            u = par[u][i];
        }
    }
    if(u == v)
        return ansid;
    for(int i = maxlog - 1 ; i >= 0 ; i -- )
    {
        if(par[u][i] != par[v][i])
        {
            if(ans < maxi[u][i]){
                ans = maxi[u][i];
                ansid = maxid[u][i];
            }
            if(ans < maxi[v][i]){
                ans = maxi[v][i];
                ansid = maxid[v][i];
            }
            u = par[u][i];
            v = par[v][i];
        }
    }
    if(ans < maxi[u][0]){
        ans = maxi[u][0];
        ansid = maxid[u][0];
    }
    if(ans < maxi[v][0]){
        ans = maxi[v][0];
        ansid = maxid[v][0];
    }
    return ansid;
}
vector<int> mstedges;
int main()
{
    ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0); cout.precision(20);
    int n , m ;
    cin >> n >> m;
    for(int i = 0 ; i < m ; i ++ )
        cin >> a[i].w;
    for(int i = 0 ; i < m ; i ++ )
        cin >> a[i].c;
    for(int i = 0 ; i < m ; i ++ ){
        cin >> a[i].u >> a[i].v;
        a[i].id = i + 1;
    }
    int s;
    cin >> s;
    sort(a , a + m , cmp);
    for(int i = 1 ; i <= n ; i ++ )
        comp[i] = i;
    ll tot = 0;
    for(int i = 0 ; i < m ; i ++ ){
        int p1 = getcomp(a[i].u) , p2 = getcomp(a[i].v);
        if(p1 == p2)
            continue;
        comp[p1] = p2;
        adj[a[i].v].pb(i);
        adj[a[i].u].pb(i);
        mstedges.pb(i);
        tot += (ll) a[i].w;
    }
    dfs(1 , 0 , 0);
    ll ans = (ll)1e18;
    int pos = 0 , me = 0;
    for(int i = 0 ; i < m ; i ++ ){
        int maxpl = get(a[i].v , a[i].u);
        ll cur = tot - (ll)a[maxpl].w + (ll)a[i].w - (ll)s / (ll)a[i].c;
        if(ans > cur){
            pos = i , me = maxpl;
            ans = cur;
        }
    }
    cout << ans << endl;
    cout << a[pos].id << " " << a[pos].w - s / a[pos].c << endl;
    for(auto i : mstedges){
        if(i == me)
            continue;
        cout << a[i].id << " " << a[i].w << endl;
    }
    return 0;
}
