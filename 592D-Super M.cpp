
/// link to problem : https://codeforces.com/problemset/problem/592/D

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 2e5 + 5;
const ll inf = 1e18;
const int mod = 1e9+7;
vector<int> edge[maxn];
bool mark[maxn];
bool tof[maxn];
int dis1[maxn] , dis2[maxn];
int max_dis1 = 2e5 + 3 , max_dis2 = 2e5 + 3, cnt;
void DFS(int v , int par)
{
    if(mark[v])
        tof[v] = true;
    for(int i=0;i<edge[v].size();i++){
        int u = edge[v][i];
        if(u == par)
            continue;
        dis1[u] = dis1[v] + 1;
        DFS(u , v);
        tof[v] |= tof[u];
    }
    if(tof[v]){
        cnt ++ ;
        if(dis1[v] > dis1[max_dis1] || (dis1[v] == dis1[max_dis1] && v < max_dis1))
            max_dis1 = v;
    }
}
void dfs(int v , int par)
{
    if(!tof[v])
        return ;
    if(dis2[v] > dis2[max_dis2] || (dis2[v] == dis2[max_dis2] && v < max_dis2))
        max_dis2 = v;
    for(int i=0;i<edge[v].size();i++){
        int u = edge[v][i];
        if(u == par)
            continue;
        dis2[u] = dis2[v] + 1;
        dfs(u , v);
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n , m;
    cin >> n >> m;
    for(int i=0;i<n-1;i++)
    {
        int u , v;
        cin >> u >> v;
        edge[u].pb(v) , edge[v].pb(u);
    }
    int str = -1;
    for(int i=0;i<m;i++){
        int u ;
        cin >> u;
        mark[u] = true;
        str = u;
    }
    DFS(str , 0);
    dfs(max_dis1 , 0);
    cout << min(max_dis1 , max_dis2) << endl;
    cout << 2 * (cnt - 1) - dis2[max_dis2];
    return 0;
}
