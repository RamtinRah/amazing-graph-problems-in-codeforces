///link to problem : https://codeforces.com/problemset/problem/543/B


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<pii>::iterator setit;
const int maxn = 3e3+4;
const int inf=1e9;
const int mod=1e9+7;
vector<int> edge[maxn];
int dis[maxn][maxn];
void BFS(int s)
{
    for(int i=0;i<maxn;i++)
        dis[s][i] = inf;
    queue<int> q;
    q.push(s);
    dis[s][s] = 0 ;
    while(!q.empty())
    {
        int v = q.front();
        //cout << s << " " << v << " " << dis[s][v] << endl;
        for(int i=0;i<edge[v].size();i++)
        {
            int u = edge[v][i];
            if(dis[s][u] == inf)
            {
                dis[s][u] = dis[s][v] + 1;
                q.push(u);
            }
        }
        q.pop();
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n,m; cin >> n >> m;
    for(int i=0;i<m;i++)
    {
        int u,v; cin >> u >> v;
        edge[u].pb(v);
        edge[v].pb(u);
    }
    int s1,t1,l1,s2,t2,l2; cin >> s1 >> t1 >> l1 >> s2 >> t2 >> l2;
    for(int i=1;i<=n;i++)
        BFS(i);
    if(dis[s1][t1] > l1 || dis[s2][t2] > l2)
    {
        cout << -1;
        return 0;
    }
    int ans = dis[s1][t1] + dis[s2][t2];
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
        {
            if(dis[s1][i] + dis[i][j] + dis[j][t1] <= l1 && dis[s2][i] + dis[i][j] + dis[j][t2] <= l2)
                ans = min(ans , dis[s1][i] + dis[i][j] + dis[j][t1] + dis[s2][i] + dis[j][t2]);
            swap(s1,t1);
            if(dis[s1][i] + dis[i][j] + dis[j][t1] <= l1 && dis[s2][i] + dis[i][j] + dis[j][t2] <= l2)
                ans = min(ans , dis[s1][i] + dis[i][j] + dis[j][t1] + dis[s2][i] + dis[j][t2]);
            swap(s1,t1);
        }
    }
    cout << m - ans;
    return 0;
}
