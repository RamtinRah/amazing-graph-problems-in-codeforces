///link to problem : https://codeforces.com/problemset/problem/29/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 500 + 4;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
int n , m;
int ans[maxn][maxn][2];
pii par[maxn][maxn][2];
queue<pair<pii,bool> > q;
vector<int> edge[maxn];
void solve(){
    while(!q.empty()){
        int u = q.front().first.first;
        int v = q.front().first.second;
        bool b = q.front().second;
        q.pop();
        if(b == 0){
            for(int i=0;i<edge[u].size();i++){
                int k = edge[u][i];
                if(!ans[k][v][1]){
                    par[k][v][1] = mp(u , v);
                    ans[k][v][1] = ans[u][v][0] + 1;
                    q.push(mp(mp(k , v) , 1));
                }
            }
        }
        else{
            for(int i=0;i<edge[v].size();i++){
                int k = edge[v][i];
                if(!ans[u][k][0] && k != u){
                    par[u][k][0] = mp(u , v);
                    ans[u][k][0] = ans[u][v][1] + 1;
                    q.push(mp(mp(u , k) , 0));
                }
            }
        }
    }
}
vector<pii> res;
void print(int x , int y , bool b){
    if(x == 0 && y == 0)
        return ;
    res.pb(mp(x , y));
    print(par[x][y][b].first , par[x][y][b].second , b ^ 1);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n >> m;
    for(int i=0;i<m;i++){
        int u , v; cin >> u >> v;
        edge[u].pb(v) , edge[v].pb(u);
    }

    ans[1][n][0] = 1;
    q.push(mp(mp(1,n),0));

    solve();
    if(ans[n][1][0] / 2 == 0){
        cout << -1;
        return 0;
    }
    cout << ans[n][1][0] / 2 << endl;
    print(n , 1 , 0);
    reverse(res.begin() , res.end());
    for(int i=0;i<res.size();i+=2)
        cout << res[i].first << " ";
    cout << endl;
    for(int i=0;i<res.size();i+=2)
        cout << res[i].second << " ";
    return 0;
}
