///link to problem : https://codeforces.com/problemset/problem/507/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 22;
const int mod = 1e9 + 7;
const int sq = 350;
vector<pair<int,bool> > edge[maxn];
int n , m;
bool mark[maxn];
pair<int,int> dis[maxn];
pair<int,bool> pre[maxn];
map<pii,bool> my_map;
vector<pair<pii,bool> > edges , ans;
void bfs(){
    mark[1] = true;
    queue<int> q;
    q.push(1);
    dis[1] = mp(0 , 0);
    while(!q.empty()){
        int v = q.front();
        q.pop();
        for(int i=0;i<edge[v].size();i++){
            int u = edge[v][i].first;
            bool b = edge[v][i].second;
            if(!mark[u]){
                dis[u] = mp(dis[v].first + 1 , dis[v].second + b);
                q.push(u);
                mark[u] = true;
                pre[u] = mp(v , b);
            }
            else if(dis[v].first + 1 == dis[u].first && dis[u].second < dis[v].second + b){
                dis[u].second = dis[v].second + b;
                pre[u] = mp(v , b);
            }
        }
    }
}
void func(int v){
    if(v == 1)
        return ;
    int u = pre[v].first;
    my_map[mp(u , v)] = true;
    bool b = pre[v].second;
    if(!b)
        ans.pb(mp(mp(u , v) , 1));
    func(u);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n >> m;
    for(int i=0;i<m;i++){
        int u , v;
        bool b;
        cin >> u >> v >> b;
        edge[u].pb(mp(v , b));
        edge[v].pb(mp(u , b));
        edges.pb(mp(mp(u , v) , b));
    }
    bfs();
    func(n);
    for(int i=0;i<edges.size();i++){
        int u = edges[i].first.first;
        int v = edges[i].first.second;
        bool b = edges[i].second;
        if(!my_map[mp(u , v)] && !my_map[mp(v , u)] && b)
            ans.pb(mp(mp(u , v) , 0));
    }
    cout << ans.size() << endl;
    for(int i=0;i<ans.size();i++)
        cout << ans[i].first.first << " " << ans[i].first.second << " " << ans[i].second << endl;
    return 0;
}
