///link to problem : https://codeforces.com/problemset/problem/875/F


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 2e5 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = 1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;

struct vertex{
    int u , v , w;
};
bool cmp(vertex a , vertex b){
    return a.w > b.w;
}
bool ok[maxn];
int par[maxn];
int get_par(int v){
    if(v == par[v])
        return v;
    return par[v] = get_par(par[v]);
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    int n , m;
    cin >> n >> m;
    vector<vertex> vec;
    for(int i = 0 ; i < m ; i ++ ){
        int u , v , w;
        cin >> u >> v >> w;
        vertex cur = {u , v , w};
        vec.pb(cur);
    }
    for(int i = 1 ; i <= n ; i ++ ){
        par[i] = i;
        ok[i] = true;
    }
    sort(vec.begin() , vec.end() , cmp);
    int ans = 0;
    for(int i = 0 ; i < m ; i ++ ){
        int u = vec[i].u , v = vec[i].v , w = vec[i].w;
        int p1 = get_par(u);
        int p2 = get_par(v);
        if(ok[p1]){
            ans += w;
            par[par[u]] = p2;
        }
        else if(ok[p2]){
            ans += w;
            par[par[v]] = p1;
        }
        if(p1 == p2){
            ok[p1] = false;
            ok[p2] = false;
        }
    }
    cout << ans;
    return 0;
}
