
///link to problem : https://codeforces.com/problemset/problem/160/D




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e5+6;
const int maxm = 1e6+7;
const ll inf = 1e18;
const int mod = 1e9+7;
int par[maxn] , ans[maxn];
struct edge{
    int u , v , ind;
};
vector<edge> v[maxm];
vector<pii> adj[maxn];
bool mark[maxn] , dfs_edge[maxn];
int depth[maxn] , num[maxn] , sum[maxn];
int get_par(int v)
{
    if(par[v] == v)
        return v;
    return par[v] = get_par(par[v]);
}
void dfs(int v , int pr)
{
    mark[v] = true;
    for(int i=0;i<adj[v].size();i++)
    {
        int u = adj[v][i].first;
        int ind = adj[v][i].second;
        if(!mark[u])
        {
            //cout << "a" << endl;;
            dfs_edge[ind] = true;
            depth[u] = depth[v] + 1;
            dfs(u,v);
        }
    }
}
void dfs2(int v)
{
    mark[v] = true;
    sum[v] = num[v];
    for(int i=0;i<adj[v].size();i++)
    {
        int u = adj[v][i].first;
        int ind = adj[v][i].second;
        if(!mark[u])
        {
            dfs2(u);
            //cout << "b" << endl;
            if(sum[u] == 0)
                ans[ind] = 1;
            sum[v] += sum[u];
        }
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n , m ; cin >> n >> m;
    for(int i=0;i<n;i++)
        par[i] = i;
    for(int i=0;i<m;i++)
    {
        edge x;
        int w;
        cin >> x.u >> x.v >> w;
        x.u -- , x.v -- ;
        x.ind = i;
        v[w].pb(x);
    }
    for(int i=0;i<maxm;i++)
    {
        for(int j=0;j<v[i].size();j++)
        {
            int par1 = get_par(v[i][j].u);
            int par2 = get_par(v[i][j].v);
            int ind = v[i][j].ind;
            if(par1 == par2)
            {
                ans[ind] = -1;
                continue ;
            }
            adj[par1].pb(mp(par2,ind));
            adj[par2].pb(mp(par1,ind));
        }
        for(int j=0;j<v[i].size();j++)
        {
            int par1 = get_par(v[i][j].u);
            if(!mark[par1])
                dfs(par1 , -1);
            int par2 = get_par(v[i][j].v);
            if(!mark[par2])
                dfs(par2 , -1);
        }
        for(int j=0;j<v[i].size();j++)
        {
            int par1 = get_par(v[i][j].u);
            int par2 = get_par(v[i][j].v);
            int ind = v[i][j].ind;
            if(depth[par1] > depth[par2])
                swap(par1 , par2);
            mark[par1] = false;
            mark[par2] = false;
            if(dfs_edge[ind])
                continue ;
            num[par2] ++ ;
            num[par1] -- ;
        }
        for(int j=0;j<v[i].size();j++)
        {
            int par1 = get_par(v[i][j].u);
            int par2 = get_par(v[i][j].v);
            if(!mark[par1])
                dfs2(par1);
            if(!mark[par2])
                dfs2(par2);
        }
        for(int j=0;j<v[i].size();j++)
        {
            int par1 = get_par(v[i][j].u);
            int par2 = get_par(v[i][j].v);
            mark[par1] = false , mark[par2] = false;
            adj[par1].clear() , adj[par2].clear();
            num[par1] = 0 , num[par2] = 0;
            sum[par1] = 0 , sum[par2] = 0;
            depth[par1] = 0 , depth[par2] = 0;
            par[par1] = par2;
        }
    }
    for(int i=0;i<m;i++)
    {
        if(ans[i] == 1)
            cout << "any";
        if(ans[i] == 0)
            cout << "at least one";
        if(ans[i] == -1)
            cout << "none";
        cout << endl;
    }
    return 0;
}
