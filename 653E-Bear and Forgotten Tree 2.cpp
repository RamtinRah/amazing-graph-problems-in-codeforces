///link to problem : https://codeforces.com/problemset/problem/653/E




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e5 + 2;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int sq = 350;
vector<int> adj[maxn];
bool mark[maxn];
set<int> un_used , st;
void bfs(int v){
    mark[v] = true;
    un_used.erase(v);
    queue<int> q;
    q.push(v);
    while(!q.empty()){
        int u = q.front();
        q.pop();
        for(auto w : adj[u])
            un_used.erase(w);
        for(set<int> :: iterator it = un_used.begin() ; it != un_used.end() ; it ++ ){
            int w = *it;
            q.push(w);
            mark[w] = true;
        }
        un_used.clear();
        for(auto w : adj[u])
            if(!mark[w])
                un_used.insert(w);
    }
}
int main()
{
	ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0);
	int n , m , k;
	cin >> n >> m >> k;
	for(int i = 2 ; i <= n ; i ++ )
        st.insert(i) , un_used.insert(i);
	for(int i = 0 ; i < m ; i ++ ){
        int u , v;
        cin >> u >> v;
        if(u > v)
            swap(u , v);
        if(u == 1)
            st.erase(v);
        else
            adj[u].pb(v) , adj[v].pb(u);
	}
	if(st.size() < k){
        cout << "impossible";
        return 0;
	}
	int cnt = 0;
	for(set<int> :: iterator it = st.begin() ; it != st.end() ; it ++ ){
        int x = *it;
        if(!mark[x]){
            bfs(x);
            cnt ++ ;
        }
	}
	if(cnt <= k && (!un_used.size()))
        cout << "possible";
    else
        cout << "impossible";
	return 0;
}
