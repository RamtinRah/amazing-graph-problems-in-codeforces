///link to problem : https://codeforces.com/problemset/problem/73/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e6 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+7;
vector<int> edge[maxn];
bool mark[maxn];
int cnt = 0 , comps = 0 , have = 0;
void dfs(int v){
    mark[v] = true;
    cnt ++ ;
    for(int i=0;i<edge[v].size();i++){
        int u = edge[v][i];
        if(!mark[u])
            dfs(u);
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , k , m;
    cin >> n >> m >> k;
    for(int i=0;i<m;i++){
        int u , v;
        cin >> u >> v;
        edge[u].pb(v) , edge[v].pb(u);
    }
    for(int i=1;i<=n;i++){
        if(mark[i])
            continue;
        cnt = 0;
        comps ++ ;
        dfs(i);
        have += min(k , cnt);
    }
    if(k == 1)
        cout << max(0 , comps - 2);
    else
        cout << max(0 , comps - 1 - have / 2);
    return 0;
}
