

///link to problem : https://codeforces.com/problemset/problem/687/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 1e6 + 43;
const int maxlog = 60;
const ll mod = 1e18 + 7;
const int sq = 1000 ;
const int inf = 1e9 + 43;
const ld pi = 3.1415926535897932384626433832795028841971693993751;
struct edge{
    int u , v , w , id;
} a[maxn];
bool cmp(edge x , edge y){
    return x.w > y.w;
}
int color[maxn] , par[maxn];
vector<int> c[maxn];
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20) ;
    int n , m , q;
    cin >> n >> m >> q;
    for(int i = 0 ; i < m ; i ++ ){
        cin >> a[i].u >> a[i].v >> a[i].w;
        a[i].id = i + 1;
    }
    sort(a , a + m , cmp);
    while(q -- ){
        int l , r ;
        cin >> l >> r;
        for(int i = 1 ; i <= n ; i ++ ){
            color[i] = -1 ;
            par[i] = i;
            c[i].clear();
            c[i].pb(i);
        }
        int ans = -1;
        for(int i = 0 ; i < m ; i ++ ){
            if(a[i].id >= l && a[i].id <= r){

                int u = a[i].u , v = a[i].v;
                //cout << "*    " << u << " " << v << endl;
                int p1 = par[u] , p2 = par[v];
                //cout << "*    " << u << " " << v << " " << par[u] << " " << par[v] << endl;
                if(p1 == p2){
                    if(color[u] == color[v]){
                        ans = a[i].w;
                        break ;
                    }
                }
                else{
                    if(color[u] == -1 && color[v] == -1){
                        color[u] = 0;
                        color[v] = 1;
                        par[u] = p2;
                        c[p2].pb(u);
                    }
                    else if(color[u] == -1){
                        color[u] = color[v] ^ 1;
                        par[u] = p2;
                        c[p2].pb(u);
                    }
                    else if(color[v] == -1){
                        color[v] = color[u] ^ 1;
                        par[v] = p1;
                        c[p1].pb(v);
                    }
                    else{
                        bool tof = false;
                        if(color[u] == color[v])
                            tof = true;
                        if(c[p1].size() < c[p2].size()){
                            swap(p1 , p2);
                            swap(u , v);
                        }
                        for(int i = 0 ; i < c[p2].size() ; i ++ ){
                            int v2 = c[p2][i];
                            par[v2] = p1 ;
                            c[p1].pb(v2);
                            color[v2] ^= tof;
                        }
                    }

                }
            }
        }
        cout << ans << endl;
    }
    return 0;
}
