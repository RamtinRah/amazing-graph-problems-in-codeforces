/// link to problem : https://codeforces.com/problemset/problem/723/F

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 4;
const int maxlog = 20;
const int mod = 1000000007;
const int sq = 720;
int n , m , s , t , ds , dt;
vector<pii> ans , edges;
int par[maxn];
vector<int> vec[maxn];
int get_par(int v){
    if(v == par[v])
        return v;
    return par[v] = get_par(par[v]);
}
map<pii , bool> adj;
int v1[maxn] , v2[maxn];
int main()
{
	ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0); cout.precision(20);
	cin >> n >> m;
	for(int i = 1 ; i <= n ; i ++ )
        par[i] = i;

	for(int i = 0 ; i < m ; i ++ ){
        int u , v;
        cin >> u >> v;
        edges.pb(mp(u , v));
        adj[mp(u , v)] = true;
        adj[mp(v , u)] = true;
	}
	cin >> s >> t >> ds >> dt;
	for(int i = 0 ; i < m ; i ++ ){
        int u = edges[i].first , v = edges[i].second;
        if(u == s || u == t || v == s || v == t)
            continue;
        int pu = get_par(u) , pv = get_par(v);
        if(pu != pv){
            ans.pb(mp(u , v));
            par[pu] = pv;
        }
	}
	for(int i = 1 ; i <= n ; i ++ ){
        if(i != s && i != t){
            int p = get_par(i);
            vec[p].pb(i);
        }
	}
	for(int i = 1 ; i <= n ; i ++ ){
        v1[i] = -1 , v2[i] = -1;
        for(auto v : vec[i]){
            if(adj[mp(v , s)])
                v1[i] = v;
            if(adj[mp(v , t)])
                v2[i] = v;
        }
        if(!vec[i].size())
            continue;
        if(v1[i] != -1 && v2[i] != -1)
            continue;
        if(v1[i] != -1){
            ans.pb(mp(s , v1[i]));
            v1[i] = -1;
            ds -- ;
        }
        if(v2[i] != -1){
            ans.pb(mp(t , v2[i]));
            v2[i] = -1;
            dt -- ;
        }
	}
	int tof1 = -1 , tof2 = -1;
	for(int i = 1 ; i <= n ; i ++ ){
        if(v1[i] != -1){
            if(ds < dt){
                ans.pb(mp(t , v2[i]));
                dt -- ;
                v2[i] = -1;
                tof1 = v1[i];
            }
            else{
                ans.pb(mp(s , v1[i]));
                ds -- ;
                v1[i] = -1;
                tof2 = v2[i];
            }
        }
	}
	if(ans.size() != n - 1){
        if(tof1 == -1 && tof2 == -1){
            ds -- ;
            dt -- ;
            ans.pb(mp(s , t));
        }
        else if(tof1 == -1){
            dt -- ;
            ans.pb(mp(tof2 , t));
        }
        else if(tof2 == -1){
            ds -- ;
            ans.pb(mp(tof1 , s));
        }
        else{
            if(ds > dt){
                ds -- ;
                ans.pb(mp(tof1 , s));
            }
            else{
                dt -- ;
                ans.pb(mp(tof2 , t));
            }
        }
    }
	if(ds < 0 || dt < 0)
        cout << "No";
    else{
        cout << "Yes" << endl;
        for(int i = 0 ; i < ans.size() ; i ++ )
            cout << ans[i].first << " " << ans[i].second << endl;
    }

	return 0;
}


